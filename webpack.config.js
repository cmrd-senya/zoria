var path = require('path');

module.exports = {
  entry: {
      index: './src/index.js'
    },
  output: {
    filename: 'image-viewer',
    path: path.resolve(__dirname),
        libraryTarget: 'var',
        library: '[name]'
  },
    resolve: {
        modules: [
            path.resolve('./src'),
            'node_modules'
        ]
    },
    externals: {
        'gnome': 'global',
        'lang': 'imports.lang',
        'gi/meta': 'imports.gi.Meta',
        'gi/shell': 'imports.gi.Shell',
        'ui/main': 'imports.ui.main',
        'ui/popupMenu': 'imports.ui.popupMenu',
        'ui/panelMenu': 'imports.ui.panelMenu',
        'gi/atk': 'imports.gi.Atk',
        'gi/st': 'imports.gi.St',
        'gi/gtk': 'imports.gi.Gtk',
        'gi/gdk': 'imports.gi.Gdk',
        'gi/gdk-pixbuf': 'imports.gi.GdkPixbuf',
        'gi/gegl': 'imports.gi.Gegl',
        'gi/gobject': 'imports.gi.GObject',
        'gi/graphene': 'imports.gi.Graphene',
        'gi/gio': 'imports.gi.Gio',
        'gi/soup': 'imports.gi.Soup',
        'gi/glib': 'imports.gi.GLib',
        'gi/clutter': 'imports.gi.Clutter',
        'gi/versions': 'imports.gi.versions',
        'misc/config': 'imports.misc.config',
        'me': 'imports.misc.extensionUtils.getCurrentExtension()'
    }
};
