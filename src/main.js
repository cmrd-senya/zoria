const { Gtk, Gio } = imports.gi
const { RecentManager } = Gtk

import { MyImage } from './register'

import 'core-js/web/url'

const recentImages = () =>
((new RecentManager()).get_items().filter((item) => (
	item.get_mime_type().match(/^image\//)
)));

class WelcomeToTheGrid {

    // Create the application itself
    constructor() {
		this.recent = new RecentManager();
        this.application = new Gtk.Application({
            flags: Gio.ApplicationFlags.HANDLES_OPEN
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', () => {
            const lastURI = recentImages()[0].get_uri()
            const parsed = new URL(lastURI)
            const path = decodeURI(parsed.pathname)
            this._image.openPath(path)
            this._openButton.select_file(Gio.File.parse_name(lastURI))
            this.activateMainWindow()
        });
        this.application.connect('startup', this._onStartup.bind(this));
        this.application.connect('open', (_, fileArray) => {
            const filePathToOpen = fileArray[0].get_parse_name()
            this._image.openPath(filePathToOpen)
            this._openButton.select_file(Gio.File.parse_name(filePathToOpen))
            this.activateMainWindow()
        })
    }

    // Callback function for 'activate' signal presents windows when active
    activateMainWindow() {
        this.application.add_window(this._window)
        this._window.present();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI ();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            // border_width: 10,
            // title: "Welcome to the Grid"
        });

        this._window.connect('close-request', () => {
            this.application.remove_window(this._window)
        })

        // this._window.set_default_size(400, 300)

        // const gdkWindow = this._window.get_window()
        // log(gdkWindow.get_frame_extents().x)
        // log(gdkWindow.get_frame_extents().y)
        // log(gdkWindow.get_frame_extents().width)
        // log(gdkWindow.get_frame_extents().height)
        // log('XXXX')
        // this._window.show()
        // this._window.screen.get_display().get_monitor_at_window(this._window)
        // const gdkWindow = this._window.get_window()
        // const monitor = gdkWindow.get_display().get_monitor_at_window(gdkWindow)
        // log(monitor.get_workarea().x)
        // log(monitor.get_workarea().y)
        // log(monitor.get_workarea().width)
        // log(monitor.get_workarea().height)
        // this._window.hide()

        // this._window.connect('configure-event', () => {
            // const [width, height] = this._window.get_size()
            // log(width)
            // log(height)
            // // this._window.get_screen().get_display()
            // const gdkWindow = this._window.get_window()
            // const monitor = gdkWindow.get_display().get_monitor_at_window(gdkWindow)
            // const workarea = monitor.get_workarea()
            // const geometry = new Gdk.Geometry({
            //     max_width: workarea.width,
            //     max_height: workarea.height
            // })
            // // log('----')
            // // log(monitor.get_workarea().x)
            // // log(monitor.get_workarea().y)
            // // log(monitor.get_workarea().width)
            // // log(monitor.get_workarea().height)
            // // log('----')
            // // gdkWindow.maximize()
            // this._window.set_geometry_hints(null, geometry, Gdk.WindowHints.MAX_SIZE)
        // })

        // Create the Grid
        this._grid = new Gtk.Grid ({
            // column_homogeneous: true,
            // column_spacing: 20,
            row_spacing: 20 });

		this._image = new MyImage ();

        // Create a label
        const _label = new Gtk.Label ({
            label: "Welcome to GNOME, too!",
            /* margin_top: 20 */ });

        /* Create a second label
        this._labelTwo = new Gtk.Label ({
            label: "The cake is a pie." }); */

        /* Create a button
        this._button = new Gtk.Button ({
            label: "Welcome to GNOME, too!"}); */

        // Attach the images and button to the grid
		//const layout = new Layout()
		//layout.add(this._image)
        // this._image.set_size(800,600)
        const box = new Gtk.Box()
        box.set_orientation(Gtk.Orientation.VERTICAL)

        const openButton = new Gtk.FileChooserButton({
            title: 'open',
            action: Gtk.FileChooserAction.OPEN
        })
        openButton.connect('file-set', () => {
            this._image.openPath(openButton.get_file().get_parse_name())
        });

        const button1 = new Gtk.Button()
        button1.set_label('rotate left')

        button1.connect('clicked', () => {
            this._image.rotateLeft()
        });

        const button2 = new Gtk.Button()
        button2.set_label('rotate right')

        button2.connect('clicked', () => {
            this._image.rotateRight()
        });

        box.append(openButton)
        // box.append(button1)
        // box.append(button2)

        this._image.hexpand = true
        this._image.halign = Gtk.Align.CENTER
        this._image.vexpand = true
        this._image.valign = Gtk.Align.CENTER

        // this._image.connect('configure-event', () => {
        // })

		//this._grid.attach (layout,  0, 0, 2, 1);
        this._grid.attach (this._image,  1, 0, 1, 1);
        // //this._grid.attach (this._icon,   0, 1, 1, 1);
        this._grid.attach (box,  0, 0, 1, 1);

        // this._grid.attach (_label, 0, 1, 1, 1);
        // this._grid.attach (this._labelTwo, 1, 1, 1, 1);

        // this._grid.attach (this._button, 1, 1, 1, 1);

        // Add the grid to the window
        this._window.set_child(this._grid);

        this._openButton = openButton
    }

};

// Run the application
let app = new WelcomeToTheGrid ();
app.application.run (['zoria', ...ARGV]);
