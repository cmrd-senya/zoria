// const { Gtk, GdkPixbuf: { Pixbuf }, Gio } = imports.gi
import Gtk from 'gi/gtk'
import GdkPixbuf, { Pixbuf } from 'gi/gdk-pixbuf'
import { cairo_surface_create_from_pixbuf, pixbuf_get_from_surface, cairo_set_source_pixbuf, EventMask, cairo_set_source_surface } from 'gi/gdk'
import GEGL from 'gi/gegl'
import Graphene from 'gi/graphene'

GEGL.init(null);

function loadImageToBuffer(path) {
  const graph = new GEGL.Node()

  const file = graph.create_child("gegl:load")
  file.set_property("path", path)

  const bufferNode = graph.create_child("gegl:buffer-sink")

  file.link(bufferNode)

  bufferNode.process()
  return bufferNode.get_property("buffer_object");
}

function geglBufferToPixbuf(buffer) {
  const graph = new GEGL.Node()
  const source = graph.create_child("gegl:buffer-source")
  const overlay = graph.create_child("gegl:save-pixbuf")

  source.set_property("buffer", buffer)

  source.link(overlay)

  overlay.process()
  return overlay.get_property("pixbuf")
}

class MyImage extends Gtk.Widget {
  vfunc_size_allocate(width, height, baseline) {
    // log('allocate')
    // log(width)
    // log(this.resultPixbuf.width)
    // log(height)
    if (width < this.resultPixbuf.width || height < this.resultPixbuf.height) {
      this.imageScaleFactor = Math.min(width / this.resultPixbuf.width, height / this.resultPixbuf.height)
    } else {
      this.imageScaleFactor = -1
    }
    this.renderBufferToPixbuf()
    this.queue_draw()
  }

  vfunc_measure(orientation, _for_size) {
    log(this.resultPixbuf.width)
    log(this.resultPixbuf.height)
    switch (orientation) {
    case Gtk.Orientation.HORIZONTAL:
      return [
        1,
        this.resultPixbuf.width
      ]
    case Gtk.Orientation.VERTICAL:
      return [
        1,
        this.resultPixbuf.height
      ]
    }
  }

  vfunc_snapshot(snapshot) {
    if (!this.resultPixbuf) { return }

    const rect = new Graphene.Rect()
    rect.init(0, 0, this.resultPixbuf.width, this.resultPixbuf.height)

    if (this.imageScaleFactor > 0) {
      snapshot.scale(this.imageScaleFactor, this.imageScaleFactor)
    }

    const context = snapshot.append_cairo(rect)

    cairo_set_source_pixbuf(context, this.resultPixbuf, 0, 0)
    context.paint()
  }

  resetChanges() {
    this.rotateDegrees = 0
    this.imageScaleFactor = -1
  }

  renderBufferToPixbuf() {
    const graph = new GEGL.Node()

    const source = graph.create_child("gegl:buffer-source")
    const rotate = graph.create_child('gegl:rotate-on-center')
    const scale = graph.create_child('gegl:scale-size-keepaspect')
    const overlay = graph.create_child("gegl:save-pixbuf")

    source.set_property("buffer", this.sourceBuffer)
    rotate.set_property('degrees', this.rotateDegrees)

    source.link(rotate)
    rotate.link(scale)
    scale.link(overlay)

    overlay.process()
    this.resultPixbuf = overlay.get_property("pixbuf")
  }

  openPath(path) {
    this.resetChanges()
    this.sourceBuffer = loadImageToBuffer(path)
    this.resultPixbuf = geglBufferToPixbuf(this.sourceBuffer)
    this.queue_resize()
    this.queue_draw()
  }

  _init() {
      // this.resultPixbuf = pixbuf
      // log(this.resultPixbuf)
      // const newPixbuf = pixbuf_get_from_surface(this.cairoSurface, 0, 0, 800, 600)
      // super._init({pixbuf: newPixbuf});
      super._init();

      // log('r')
      // log(this.resizable)

      // this.set_events(this.get_events() | EventMask.BUTTON_PRESS_MASK | EventMask.POINTER_MOTION_MASK)
      // log(this.get_events())

      // this.connect("motion-notify-event", (event) => {
      //   log(event)
      // })
      const drag_controller = new Gtk.GestureDrag()
      drag_controller.connect('drag-begin', (_, start_x) => {
        this.queue_draw()
      })
      drag_controller.connect('drag-end', (_) => {
        this.queue_draw()
      })
      drag_controller.connect('drag-update', (_) => {
        this.queue_draw()
      })
      this.add_controller(drag_controller)
  }

  rotateLeft() {
    this.rotateDegrees += 90
    this.renderBufferToPixbuf()
    this.queue_resize()
    this.queue_draw()
  }

  rotateRight() {
    this.rotateDegrees -= 90
    this.renderBufferToPixbuf()
    this.queue_resize()
    this.queue_draw()
  }
}

export { MyImage }
