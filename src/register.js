import GObject from 'gi/gobject'

import { MyImage as _myImage } from './image'

const MyImage = GObject.registerClass(
  {
      GTypeName: 'MyImage'
  },
  _myImage
);

export { MyImage }
